## $\color{#FF00FF}{MNXW4ZTJM52XEZIK}$

##### git config --global user.
    
    user.email   user.signingKey
    user.name    user.useConfigOnly
      
##### git config --list
    
##### ssh-keygen -a 100 -b 4096 -t dsa/ecdsa/ecdsa-sk/ed25519/ed25519-sk/rsa -C commit [-f output_keyfile]
      
    -a rounds
             When saving a private key, this option specifies the number of
             KDF (key derivation function, currently bcrypt_pbkdf(3)) rounds
             used.  Higher numbers result in slower passphrase verification
             and increased resistance to brute-force password cracking (should
             the keys be stolen).  The default is 16 rounds. 
    -b bits
             Specifies the number of bits in the key to create.  For RSA keys,
             the minimum size is 1024 bits and the default is 3072 bits.  Gen‐
             erally, 3072 bits is considered sufficient.  DSA keys must be ex‐
             actly 1024 bits as specified by FIPS 186-2.  For ECDSA keys, the
             -b flag determines the key length by selecting from one of three
             elliptic curve sizes: 256, 384 or 521 bits.  Attempting to use
             bit lengths other than these three values for ECDSA keys will
             fail.  ECDSA-SK, Ed25519 and Ed25519-SK keys have a fixed length
             and the -b flag will be ignored. 
      
##### ssh -T git@github.com
    
    -4  -A  -c  -e  -F  -i  -k  -L  -n  -O  -Q  -S  -v  -W  -y 
    -6  -b  -C  -E  -g  -I  -K  -m  -N  -p  -R  -t  -V  -x  -Y 
    -a  -B  -D  -f  -G  -J  -l  -M  -o  -q  -s  -T  -w  -X
    
    -T      Disable pseudo-terminal allocation.
      
##### gpg --full-generate-key
##### gpg --list-secret-keys --keyid-format=long/short
##### gpg --list-sigs/--list-sig/--list-signatures
##### gpg --armor/--armour --export xxxxxxxxxxxx
      
    --expert                 --export-options         --export-secret-subkeys
    --export                 --export-ownertrust      --export-ssh-key
    --export-filter          --export-secret-keys 

##### gpg --send-keys xxxxxxxxxxx
##### git config --global commit.gpgsign true
##### git config --global user.signingkey key

##### git ls-files
##### git status
       
    stage    stash    status
    
##### git branch -dr main `# git push origin --delete branch`
##### git remote add user-defined(origin) git@github.com:USERNAME/UserRepo , https://github.com/USERNAME/UserRepo
       
    add            prune          rename         set-head       show 
    get-url        remove         set-branches   set-url        update 

##### git push -uf origin main `#-u,--set-upstream && -f,--force`

---
    
## $\color{#0000FF}{OJSXM2LTMUFA====}$
  
##### git rebase -i HEAD~23 `# delete.commit.23 #-i,--interactive `
##### git rebase branch -i `#change fatal: No rebase in progress `
##### git rebase add . `@git rebase --continue `
##### git rebase branch -i `#push --force`
##### git update-ref -d HEAD `#reset.commit `
    
##### git reset --soft/--mixed/--hard HEAD~1/HEAD^ `#push --force `

##### git reflog expire --expire=now --all `#clear.reflog `
##### git gc --aggressive --prune=now
##### git reflog delete HEAD@{32} HEAD@{31}
    
     delete   expire   show
      
##### git log --all --oneline --graph
##### git show-branch --more --all
##### git revert commitID
##### git checkout --orphan=newbranch

#### expired

    gpg --generate-revocation keyid
    gpg --edit-key keyid , expire , trust , save
    git config --global commit.gpgSign true
    git config --global user.signingKey keyid
    gpg --armor --output filepublic --export keyid #
    gpg --armor --output fileprivate --export-secret-keys #
    gpg --local-user privatekeyid --recipient userkeyid --armor --sign --encrypt fileprivate #generate fileprivate.asc
    gpg --verify fileprivate.asc fileprivate
---
