#!/bin/bash
#
termuxdns="nameserver 223.6.6.6 \nnameserver 1.2.4.8 \nnameserver 223.5.5.5"
termuxextrakeys="#ZXh0cmEta2V5cwo\nextra-keys = [['ESC','/','<','>','-','HOME','UP','END','PGUP'],['TAB','CTRL','{','}','ALT','LEFT','DOWN','RIGHT','PGDN']]"
termuxsourceslist="deb https://mirrors.ustc.edu.cn/termux/apt/termux-main stable main"
termuxhome="/data/data/com.termux/files/home"
termuxpro=`grep "#ZXh0cmEta2V5cwo"  $termuxhome/.termux/termux.properties`
termuxbashrc="#Y2RkCg\nexport HISTSIZE=10000\nalias cdd=\"cd \$PREFIX\""
termuxrc=`grep "#Y2RkCg" $termuxhome/.bashrc`

if [[ -f $0.or ]];then
	exit
fi
function tffile() {
	if [ ! -e $1.bat ];then
		cp $1 $1.bat
	fi
}
tffile $PREFIX/etc/apt/sources.list
echo -e $termuxsourceslist > $PREFIX/etc/apt/sources.list
tffile $PREFIX/etc/resolv.conf
pkg update -y
pkg upgrade -y
tffile $termuxhome/.termux/termux.properties
function tff(){
	if [ ! $1 ];then
		echo -e $2 >> $3
	fi
}
tff $termuxpro $termuxextrakeys $termuxhome/.termux/termux.properties
tff $termuxrc $termuxbashrc $termuxhome/.bashrc
`source $termuxhome/.bashrc`
stat $0 > $0.or
