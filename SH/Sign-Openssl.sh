#!/bin/bash
#
dirnew="./_$0"
datafile="$dirnew/_$0"
sdatafile="$dirnew/__$0"
topopenssl="openssl dgst -sha256"
if [ ! -d $dirnew ];then
	mkdir -v $dirnew
fi
if [ ! -f $datafile ];then
	find -maxdepth 1 -type f |grep -v "$0" > $datafile
fi
allfiles=`cat $datafile`
num=1
function eenc(){
	openssl enc -e -aes-128-cbc -in $1 -out $2 -pass pass:1 -pbkdf2
}
function denc(){
	openssl enc -d -aes-128-cbc -in $1 -out $2 -pass pass:1 -pbkdf2
}
function zgenrsa(){
	openssl genrsa -out $1 4096
}
for ff in $allfiles;do
	if [ -f $dirnew/${ff##*/} ];then
		eenc ${ff} $dirnew/${ff##*/}$((num++))
	fi
		eenc ${ff} $dirnew/${ff##*/}
done
if [ ! -f $sdatafile ];then
	find $dirnew/ -maxdepth 1 -type f | grep -v $datafile | grep -v $sdatafile > $sdatafile
fi
sallfiles=`cat $sdatafile`
for ddfir in $sallfiles;do

	prikeyn="${ddfir##*/}_privatek"
	outsignpri="${ddfir##*/}_signpri"
	statfile="${ddfir##*/}_stat"
if [[ ! -f $dirnew/$prikeyn ]];then
	zgenrsa $dirnew/$prikeyn
	stat ${ddfir##*/} > $dirnew/$statfile
fi
if [[ ! -f $dirnew/$outsignpri ]];then
	$topopenssl -sign $dirnew/$prikeyn -out $dirnew/$outsignpri $dirnew/${ddfir##*/}
fi
dff="$dirnew/${ddfir##*/} $dirnew/$prikeyn $dirnew/$statfile $dirnew/$outsignpri"
if [ ! -f $dirnew/${ddfir##*/}.tar ];then
	tar -cvf $dirnew/${ddfir##*/}.tar $dff --remove-files 1>/dev/null
fi 
	tar -xvf $dirnew/${ddfir##*/}.tar --force-local 2>&1
	$topopenssl -prverify $dirnew/$prikeyn -signature $dirnew/$outsignpri $dirnew/${ddfir##*/}
for df in $dff;do
	if [[ -f $df ]];then
		rm -rf $df
	fi
done
done
echo -e "......DENC.......\n"
tmp="$dirnew/tmp"
ntmp="$dirnew/tmp/_$0"
mkdir -vp $tmp
find $dirnew -maxdepth 1 -type f -name "*.tar" > $dirnew/_tmp
tartmp=`cat $dirnew/_tmp`
for aa in $tartmp;do
	tar -xvf $dirnew/${aa##*/} -C $tmp/ 2>&1
	name=${aa##*/}
	pri=`ls $ntmp/|grep "privatek"`
	sign=`ls $ntmp/|grep "signpri"`
	$topopenssl -prverify $ntmp/$pri -signature $ntmp/$sign $ntmp/${name%.*}
	denc $ntmp/${name%.*} $ntmp/${name%.*}.data
	rm $ntmp/${name%.*} $ntmp/$pri $ntmp/$sign
done
