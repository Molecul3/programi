#!/bin/bash
echo "username,\$1 SOURCE,\$2 DIRECTORY,\$3"
if [[ -z $1 ]];then
	exit
fi
serverip=""
serverport=22
if [[ $(uname -m) == "x86_64" ]];then
	sshkeydir="~/.ssh/know_hosts"
else
	sshkeydir="/data/data/com.termux/files/home/.ssh/known_hosts"
fi
errip=`awk '{print $1}' $sshkeydir | uniq | grep "${serverip}"`
if [[ $errip ]];then
	sed -i "/${serverip}/d" $sshkeydir
fi
while true; do
    read -p "SELECT,Connetc[cC] Up[uU \$2\$3] Download[dD \$2\$3] " ud
    case $ud in
        [cC] ) ssh $1@$serverip -v -p $serverport; break;;
        [uU] ) scp -v -r -P $serverport $2 $1@$serverip:$3;break;;
	[dD] ) scp -v -r -P $serverport $1@$serverip:$2 $3;break;;
        * ) echo "Please SELECT.";;
    esac
done
