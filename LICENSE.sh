#!/bin/bash
# $1,oldfile $2,maxdepth
if [[ -z $1 || -z $2 ]];then
	echo -e "\$1,\$2"
	exit 2
fi
outf="_$0"
olistf="$1"
tmpdate="_tmp.date"
sess=1
looc=1
find -maxdepth $2 -type f |grep -v $outf |grep -v $tmpdate > $outf
listf=`cat $outf`
function sear(){
	cat ${olistf} |grep -w $1|awk "NR==1{print \$$2,\$$3}" >> $tmpdate
}
function rdate(){
	date --date="@$1" +%k:%M:%S
}
for dd in $listf;do
IFS=
	x=`stat -c %X $dd`
	y=`stat -c %Y $dd`
	z=`stat -c %Z $dd`
if [[ $(uname -m) == "x86_64" ]];then
	sear ${dd} 33 34
	sear ${dd} 37 38
	sear ${dd} 41 42
fi
if [[ $(uname -m) == "aarch64" ]];then
	sear ${dd} 29 30
	sear ${dd} 33 34
	sear ${dd} 37 38
fi
tmpx=`cat $tmpdate|sed -n '1p'`
tmpy=`cat $tmpdate|sed -n '2p'`
tmpz=`cat $tmpdate|sed -n '3p'`
	
dtmpx=`date -d "${tmpx}" +%s`
dtmpy=`date -d "${tmpy}" +%s`
dtmpz=`date -d "${tmpz}" +%s`
	
if [ $((x-dtmpx)) -gt $sess ] || [ $((x-dtmpx)) -gt $sess ] | [ $((x-dtmpx)) -gt $sess ];then

echo $((x-dtmpx))"s" $dd $(rdate $x)
fi
	ww=$((looc++))
	if [ $((ww % 100)) -eq 0 ];then
		echo -e "\t$ww"
	fi
	cat $tmpdate > $tmpdate
done
rm $tmpdate $outf
